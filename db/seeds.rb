# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
require 'faker'

User.create(name:"João Souza", nacionality: "brasileiro", role:0, state:"Rio de Janeiro", rg:"1234567", cpf:"1", email:"joao@gmail.com",enroll:"1567890", password:"123456", birthday:"02/04/1998")
User.create(name:"Maria Borges", nacionality: "brasileiro", role:1, state:"Ceará", rg:"1234568", cpf:"2", email:"maria@gmail.com",enroll:"1567891", password:"123456", birthday:"23/06/2000")
User.create(name:"Ricardo Silva", nacionality: "brasileiro", role:2, state:"Recife", rg:"1234569", cpf:"3", email:"ricardo@gmail.com",enroll:"1567892", password:"123456", birthday:"02/10/1991")
User.create(name:"Elaine Cardoso", nacionality: "brasileiro", role:3, state:"São Paulo", rg:"1234560", cpf:"4", email:"elaine@gmail.com",enroll:"1567893", password:"123456", birthday:"08/09/1995")
User.create(name:"Pedro Rabelo", nacionality: "brasileiro", role:4, state:"Rio de Janeiro", rg:"1234561", cpf:"5", email:"pedro@gmail.com",enroll:"1567894", password:"123456", birthday:"06/11/1998")

cont = 0
while cont < 5
    Department.create(name: Faker::Educator.degree) do |departments|
        departments.knowledge_field = Faker::Educator.subject
        departments.code = Faker::Bank.account_number(digits: 7)
        departments.campus_address = Faker::Educator.campus
    end
    cont +=1
end

a = 0
while a < 20
    User.create(role: 1) do |users|
        users.name = Faker::Name.name 
        users.nacionality = Faker::Nation.nationality
        users.state = Faker::Address.state
        users.rg = Faker::IDNumber.brazilian_id(formatted: true)
        users.cpf = Faker::IDNumber.brazilian_citizen_number(formatted: true)
        users.email = Faker::Internet.free_email
        users.password = Faker::Internet.password(min_length: 6)
        users.birthday = Faker::Date.birthday(min_age: 18, max_age: 80)
        users.enroll = Faker::Bank.account_number(digits: 7)

    end
    a +=1
end

b = 0
while b < 20
    User.create(role: 0) do |users|
        users.name = Faker::Name.name 
        users.nacionality = Faker::Nation.nationality
        users.state = Faker::Address.state
        users.rg = Faker::IDNumber.brazilian_id(formatted: true)
        users.cpf = Faker::IDNumber.brazilian_citizen_number(formatted: true)
        users.email = Faker::Internet.free_email
        users.password = Faker::Internet.password(min_length: 6)
        users.birthday = Faker::Date.birthday(min_age: 18, max_age: 80)
        users.enroll = Faker::Bank.account_number(digits: 7)
    end
    b +=1
end

c = 0
while c < 5 
    Course.create(name: Faker::Educator.subject ) do |courses|
        courses.knowledge_field = Faker::Educator.subject
        courses.code = Faker::Bank.account_number(digits: 7)
        courses.campus_address = Faker::Educator.campus
    end
    c +=1
end

d = 0
while d < 10
    Discipline.create(name: Faker::Educator.course_name ) do |disciplines|
        disciplines.workload = Faker::Bank.account_number(digits: 2)
        disciplines.knowledge_field = Faker::Educator.subject
    end
    d +=1
end


e = 0
while e < 30
    Team.create(name: Faker::Educator.course_name) do |teams|
        teams.code = Faker::Alphanumeric.alphanumeric(number: 2, min_alpha: 1, min_numeric: 1)
        teams.calendar = Faker::Date.in_date_period
        teams.room = Faker::Bank.account_number(digits: 3)
    end
    e +=1
end

