class AddSubjectidToDisciplines < ActiveRecord::Migration[6.1]
  def change
    add_reference :disciplines, :subject, foreign_key: true
  end
end
