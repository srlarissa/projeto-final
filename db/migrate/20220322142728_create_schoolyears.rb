class CreateSchoolyears < ActiveRecord::Migration[6.1]
  def change
    create_table :schoolyears do |t|
      t.date :year
      t.integer :status
      t.integer :semester

      t.timestamps
    end
  end
end
