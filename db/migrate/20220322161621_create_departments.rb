class CreateDepartments < ActiveRecord::Migration[6.1]
  def change
    create_table :departments do |t|
      t.string :name
      t.string :knowledge_field
      t.string :code
      t.string :campus_address

      t.timestamps
    end
  end
end
