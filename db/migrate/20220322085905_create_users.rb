class CreateUsers < ActiveRecord::Migration[6.1]
  def change
    create_table :users do |t|
      t.string :name
      t.string :nacionality
      t.string :state
      t.string :rg
      t.string :cpf
      t.string :email
      t.string :password
      t.date :birthday
      t.integer :role
      t.integer :enroll

      t.timestamps
    end
  end
end
