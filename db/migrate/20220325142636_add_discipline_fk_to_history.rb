class AddDisciplineFkToHistory < ActiveRecord::Migration[6.1]
  def change
    add_reference :histories, :discipline, null: false, foreign_key: true
  end
end
