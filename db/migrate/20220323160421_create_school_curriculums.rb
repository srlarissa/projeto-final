class CreateSchoolCurriculums < ActiveRecord::Migration[6.1]
  def change
    create_table :school_curriculums do |t|
      t.references :course, null: false, foreign_key: true
      t.references :discipline, null: false, foreign_key: true

      t.timestamps
    end
  end
end
