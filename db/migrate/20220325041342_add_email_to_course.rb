class AddEmailToCourse < ActiveRecord::Migration[6.1]
  def change
    add_column :courses, :email, :string
  end
end
