class AddAvgToTeams < ActiveRecord::Migration[6.1]
  def change
    add_column :teams, :avg, :integer, default: 0
  end
end
