class AddEnrollToUser < ActiveRecord::Migration[6.1]
  def change
    add_column :users, :enroll, :integer
  end
end
