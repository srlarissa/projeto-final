class AddGradeToTeams < ActiveRecord::Migration[6.1]
  def change
    add_column :teams, :first_grade, :integer
    add_column :teams, :second_grade, :integer
  end
end
