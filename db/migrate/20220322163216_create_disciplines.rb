class CreateDisciplines < ActiveRecord::Migration[6.1]
  def change
    create_table :disciplines do |t|
      t.string :name
      t.integer :workload
      t.string :knowledge_field
      
      t.timestamps
    end
  end
end
