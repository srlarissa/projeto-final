class CreateTeams < ActiveRecord::Migration[6.1]
  def change
    create_table :teams do |t|
      t.string :name
      t.string :code
      t.datetime :calendar
      t.integer :room

      t.timestamps
    end
  end
end
