class CreateHistories < ActiveRecord::Migration[6.1]
  def change
    create_table :histories do |t|
      t.string :discipline
      t.integer :status
      t.integer :avg

      t.timestamps
    end
  end
end
