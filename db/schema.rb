# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2022_03_25_161931) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "courses", force: :cascade do |t|
    t.string "name"
    t.string "knowledge_field"
    t.string "code"
    t.string "campus_address"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "email"
    t.string "phone"
  end

  create_table "departments", force: :cascade do |t|
    t.string "name"
    t.string "knowledge_field"
    t.string "code"
    t.string "campus_address"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "user_id"
    t.index ["user_id"], name: "index_departments_on_user_id"
  end

  create_table "disciplines", force: :cascade do |t|
    t.string "name"
    t.integer "workload"
    t.string "knowledge_field"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "subject_id"
    t.bigint "schoolyear_id"
    t.index ["schoolyear_id"], name: "index_disciplines_on_schoolyear_id"
    t.index ["subject_id"], name: "index_disciplines_on_subject_id"
  end

  create_table "histories", force: :cascade do |t|
    t.string "discipline"
    t.integer "status"
    t.integer "avg"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "discipline_id", null: false
    t.index ["discipline_id"], name: "index_histories_on_discipline_id"
  end

  create_table "school_curriculums", force: :cascade do |t|
    t.bigint "course_id", null: false
    t.bigint "discipline_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["course_id"], name: "index_school_curriculums_on_course_id"
    t.index ["discipline_id"], name: "index_school_curriculums_on_discipline_id"
  end

  create_table "schoolyears", force: :cascade do |t|
    t.date "year"
    t.integer "status"
    t.integer "semester"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "teams", force: :cascade do |t|
    t.string "name"
    t.string "code"
    t.datetime "calendar"
    t.integer "room"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "first_grade"
    t.integer "second_grade"
    t.integer "avg", default: 0
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "nacionality"
    t.string "state"
    t.string "rg"
    t.string "cpf"
    t.string "email"
    t.string "password_digest"
    t.date "birthday"
    t.integer "role"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "enroll"
  end

  add_foreign_key "departments", "users"
  add_foreign_key "disciplines", "schoolyears"
  add_foreign_key "histories", "disciplines"
  add_foreign_key "school_curriculums", "courses"
  add_foreign_key "school_curriculums", "disciplines"
end
