require "test_helper"

class CoordinatorsCourseControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get coordinators_course_index_url
    assert_response :success
  end

  test "should get show" do
    get coordinators_course_show_url
    assert_response :success
  end

  test "should get create" do
    get coordinators_course_create_url
    assert_response :success
  end

  test "should get update" do
    get coordinators_course_update_url
    assert_response :success
  end

  test "should get destroy" do
    get coordinators_course_destroy_url
    assert_response :success
  end
end
