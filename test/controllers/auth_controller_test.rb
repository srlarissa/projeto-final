require "test_helper"

class AuthControllerTest < ActionDispatch::IntegrationTest
  test "should get Login" do
    get auth_Login_url
    assert_response :success
  end

  test "should get signup" do
    get auth_signup_url
    assert_response :success
  end
end
