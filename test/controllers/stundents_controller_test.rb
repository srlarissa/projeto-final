require "test_helper"

class StundentsControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get stundents_index_url
    assert_response :success
  end

  test "should get show" do
    get stundents_show_url
    assert_response :success
  end

  test "should get create" do
    get stundents_create_url
    assert_response :success
  end

  test "should get update" do
    get stundents_update_url
    assert_response :success
  end

  test "should get destroy" do
    get stundents_destroy_url
    assert_response :success
  end
end
