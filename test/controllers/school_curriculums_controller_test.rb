require "test_helper"

class SchoolCurriculumsControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get school_curriculums_index_url
    assert_response :success
  end

  test "should get show" do
    get school_curriculums_show_url
    assert_response :success
  end

  test "should get create" do
    get school_curriculums_create_url
    assert_response :success
  end

  test "should get update" do
    get school_curriculums_update_url
    assert_response :success
  end

  test "should get destroy" do
    get school_curriculums_destroy_url
    assert_response :success
  end
end
