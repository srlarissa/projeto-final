require "test_helper"

class SchoolyearControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get schoolyear_index_url
    assert_response :success
  end

  test "should get show" do
    get schoolyear_show_url
    assert_response :success
  end

  test "should get create" do
    get schoolyear_create_url
    assert_response :success
  end

  test "should get update" do
    get schoolyear_update_url
    assert_response :success
  end

  test "should get destroy" do
    get schoolyear_destroy_url
    assert_response :success
  end
end
