Rails.application.routes.draw do
  resources :stundents
  resources :coordinators
  resources :teachers
  resources :users 
  resources :coordinators_course 

#Rotas do curriculo
resources :school_curriculums
#Rotas das matérias
resources :disciplines

#Rotas do departamento
resources :departments

#Rotas do Cursos:
resources :courses
  
#Rotas do periodo letivo:
resources :schoolyears

#Rotas do login:

post 'auth/login'
 

#Rotas das turmas:
resources :teams


end
