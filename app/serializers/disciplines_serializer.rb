class DisciplinesSerializer < ActiveModel::Serializer
  attributes :id, :name, :workload, :knowledge_field
  #criada a relação many to many de curriculo ligando materia e curso
  has_many :school_curriculums
  has_many :courses, through: :school_curriculums
  
  #criada a relação self join das materias para criação do pre requisito
  has_many :prerequisites, class_name: "Discipline",foreign_key: "subject_id"
  belongs_to :subject, class_name: "Discipline"

  #criada a relação entre disciplina e periodo letivo
  belongs_to :schoolyear
end
