class TeamSerializer < ActiveModel::Serializer
  attributes :id, :name, :code, :calendar, :room
end
