class DepartmentsSerializer < ActiveModel::Serializer
  attributes :id, :name, :knowledge_field, :code, :campus_address
end
