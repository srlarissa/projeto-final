class SchoolyearSerializer < ActiveModel::Serializer
  attributes :id, :year, :status, :semester
  has_many :disciplines
end
