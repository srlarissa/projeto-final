class CourseSerializer < ActiveModel::Serializer
  attributes :id, :name, :knowledge_field, :code, :campus_address
  has_many :school_curriculums
  has_many :disciplines, through: :school_curriculums
end
