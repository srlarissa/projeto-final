class User < ApplicationRecord
    has_many :schoolyears
    has_many :teams
    has_many :histories, through: :teams
    has_one :department

    has_secure_password

    validates :name, :nacionality, :state, :rg, :password, :birthday, :email, :role, :cpf, presence: true
    enum role:{
        student: 0, 
        teacher: 1, 
        coordinatorDP: 2, 
        coordinatorCourse: 3, 
        director: 4
    }

end
