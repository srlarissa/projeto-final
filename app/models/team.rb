class Team < ApplicationRecord
    belongs_to :user
    belongs_to :history

     def current_avg
        if (self.first_grade != nil) && (self.second_grade != nil)
            self.avg = (self.first_grade + self.second_grade)/2 
        end
     end

end
