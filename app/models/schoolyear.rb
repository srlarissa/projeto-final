class Schoolyear < ApplicationRecord
    require "date"

    belongs_to :user
    has_many :disciplines
    
    enum status:{
        planning: 0,
        subscription: 1,
        progress: 2,
        concluded: 3
    }

    enum semester:{
        s1: 0,
        s2: 1
    }

end
