class Ability
  include CanCan::Ability

  def initialize(user)

      if user.director?
        can :read, School_curriculums
        can :update, user 
        can :create, Department
        can :create, User, role: 2
        can :create, User, role: 3
        can [:read, :create, :update], Schoolyear
        cannot :create, User, role: 0
      else
        cannot :create, User, role: 0
      end
      
      
      if user.coordinatorCourse?
        can :read, School_curriculums
        can :update, user
        can :create, Course
        can [:create, :read], User, role: 0
        can [:read, :create, :update], Disciplines
      end

      if user.coordinatorDP?
        can :read, School_curriculums
        can :update, user
        can :create, Department
        can :read, Schoolyear
        can [:update, :create], Disciplines
        # can [:create, :destroy], prerequisite materia
        can [:create, :read], User, role: 1
        # can :destroy, prerequisite professor
        can [:read, :create], Team
      end

      if user.coordinatorCourse?
        can :read, School_curriculums
        can :update, user
        can :create, Course
        can [:create, :read], User, role: 0
        can [:read, :create, :update], Disciplines
      end

      if user.teacher?
        can :read, School_curriculums
        can :update, user
        can :read, Team
        # can [:create, :update], notas
      end

      if user.student?
        can :read, School_curriculums
        can :update, user
        can :read, Team
        # can :read, history
        # can :create, incrição na materia
      end



      # if user.role == 'coordinatorCourse'
      #     cannot :manage, :all 
      # end

      # if user.role == 'coordinatorDP'
      #   cannot :manage, :all 
      # end

      # if user.role == 'teacher'
      #   cannot :manage, :all 
      # end

      # if user.role == 'student'
      #   cannot :manage, :all 
      # end
  end
end
