class Course < ApplicationRecord
    has_many :school_curriculums
    has_many :disciplines, through: :school_curriculums
    validates :name, :knowledge_field, :campus_address, :code, presence: true
    
end
