class Department < ApplicationRecord
    belongs_to :coordenator, class_name: :user
    validates :name, :knowledge_field, :code, :campus_address, presence: true
end
