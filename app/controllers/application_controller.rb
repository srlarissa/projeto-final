class ApplicationController < ActionController::API
    
    def current_user
        return nil if !auth_token || !decoded_payLoad
        # User.find_by(decoded_payLoad[0]["user_id"]) #user ou nil(se não existir mais o usuário)
    end
    
    # def current_ability
    #   @current_ability ||= ::Ability.new(current_user)
    # end

    def headers
        request.headers
    end

    def auth_token
        headers["Auth-Token"]
    end

    def decoded_payLoad
        JsonWebToken::Base.decode(auth_token)
    end

end
