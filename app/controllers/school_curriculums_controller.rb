class SchoolCurriculumsController < ApplicationController
  before_action :set_sc, only:[:show, :update, :destroy]
  # load_and_authorize_resource
  def index
   @disc = Discipline.all
   render json: @disc
  end

  def show
    if @sc
      render json: @sc
    else
      render json: @sc.errors, status: 404
    end
  end

  def create
    @sc = SchoolCurriculum.new(sc_params)

    if @sc.save
      render json: @sc, status: :created
    else
      render json: @sc.errors, status: 422
    end
  end

  def update
    if @sc.update(sc_params)
      render json: @sc
    else
      render json: @sc.errors, status: 422
    end
  end

  def destroy
    @sc.destroy
  end

  private

  def course_params
    params.require(:course).permit(:name, :knowledge_field, :code, :campus_address)
  end

  def discipline_params
    params.require(:discipline).permit(:name, :knowledge_field, :workload, :subject_id)
  end

  def set_sc
    @sc = SchoolCurriculum.find(params[:id])
  end
end
