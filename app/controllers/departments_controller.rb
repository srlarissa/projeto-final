class DepartmentsController < ApplicationController
  before_action :set_dp, only:[:show, :update, :destroy]
  # load_and_authorize_resource
  def index
    @dps = Department.all
    render json: @dps
  end

  def show
    if @dp
      render json: @dp
    else
      render json: @dp.errors, status: 404
    end
  end

  def create
    @dp = Department.new(dp_params)

    if @dp.save
      render json: @dp, status: :created
    else
      render json: @dp.errors, status: 422
    end
  end

  def update
    if @dp.update(dp_params)
      render json: @dp
    else
      render json: @dp.errors, status: 422
    end
  end

  def destroy
    @dp.destroy
  end

  private

  def dp_params
    params.require(:department).permit(:name, :knowledge_field, :code, :campus_address)
  end

  def set_dp
    @dp = Department.find(params[:id])
  end
end
