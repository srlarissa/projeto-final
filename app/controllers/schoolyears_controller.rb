class SchoolyearsController < ApplicationController
  before_action :set_schoolyear, only:[:show, :update, :destroy]
  # load_and_authorize_resource
  def index
    @schoolyears = Schoolyear.all
    render json: @schoolyears
  end

  def show
    if @schoolyear
      render json: @schoolyear
    else
      render json: @schoolyear.errors, status: 404
    end
  end

  def create
    @schoolyear = Schoolyear.new(sy_params)

    if @schoolyear.save
      render json: @schoolyear, status: :created
    else
      render json: @schoolyear.errors, status: 422
    end
  end

  def update
    if @schoolyear.update(sy_params)
      render json: @schoolyear
    else
      render json: @schoolyear.errors, status: 422
    end
  end

  def destroy
    @schoolyear.destroy
  end

  private

  def sy_params
    params.require(:schoolyear).permit(:year, :status, :semester)
  end

  def set_schoolyear
    @schoolyear = Schoolyear.find(params[:id])
  end
end
