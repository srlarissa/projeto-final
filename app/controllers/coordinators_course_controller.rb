class CoordinatorsCourseController < ApplicationController 
  before_action :set_coordinator_course, only: [:show, :update, :destroy]
  # load_and_authorize_resource
  
  
  def index
    @coordinators_course = User.where(role: "coordinatorCourse")
    render json: @coordinators_course
  end

  def show    
    render json: @coordinator_course
  end

  def create
    @coordinator_course = User.new(coordinator_course_params)
    if @coordinator_course.save
      render json: @coordinator_course
    else
      render json: @coordinator_course.errors, status: 422
    end
  end

  def update
    if @coordinator_course.update(coordinator_course_params)
      render json: @coordinator_course
    else
      render json: @coordinator_course.errors, status: 422
    end
  end

  def destroy 
     
    @coordinator_course.destroy
  end
  private

  def set_coordinator_course
    @coordinator_course = User.find(params[:id])
  end

  def coordinator_course_params
    params.require(:user).permit(:name, :nacionality, :state, :rg, :password, :birthday, :email, :role, :cpf)
  end
end
