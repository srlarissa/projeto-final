class StundentsController < ApplicationController
  before_action :set_student, only: [:show, :update, :destroy]
  # load_and_authorize_resource
  def index
    @students = User.where(role: "student")
    render json: @students
  end

  def show    
    render json: @student
  end

  def create
    @student = User.new(student_params)
    if @student.save
      render json: @student
    else
      render json: @student.errors, status: 422
    end
  end

  def update
    if @student.update(student_params)
      render json: @student
    else
      render json: @student.errors, status: 422
    end
  end

  def destroy 
    @student.destroy
  end
  private

  def set_student
    @student = User.find(params[:id])
  end

  def student_params
    params.require(:user).permit(:name, :nacionality, :state, :rg, :password, :birthday, :email, :role, :cpf)
  end

end
