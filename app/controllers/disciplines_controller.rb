class DisciplinesController < ApplicationController
  before_action :set_discipline, only:[:show, :update, :destroy]
  # load_and_authorize_resource
  def index
    @disciplines = Discipline.all
    render json: @disciplines
  end

  def show
    if @discipline
      render json: @discipline
    else
      render json: @discipline.errors, status: 404
    end
  end

  def create
    @discipline = Discipline.new(discipline_params)

    if @discipline.save
      render json: @discipline, status: :created
    else
      render json: @discipline.errors, status: 422
    end
  end

  def update
    if @discipline.update(discipline_params)
      render json: @discipline
    else
      render json: @discipline.errors, status: 422
    end
  end

  def destroy
    @discipline.destroy
  end

  private

  def discipline_params
    params.require(:discipline).permit(:name, :knowledge_field, :workload, :subject_id)
  end

  def set_discipline
    @discipline = Discipline.find(params[:id])
  end
end
