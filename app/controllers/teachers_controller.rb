class TeachersController < ApplicationController
 
  # load_and_authorize_resource
  before_action :set_teacher, only: [:show, :update, :destroy]
  def index
    @teachers = User.where(role: "teacher")
    render json: @teachers
  end

  def show    
    render json: @teacher
  end

  def create
    @teacher = User.new(teacher_params)
    if @teacher.save
      render json: @teacher
    else
      render json: @teacher.errors, status: 422
      end
  end

  def update
    

    if @teacher.update(teacher_params)
      render json: @teacher
    else
      render json: @teacher.errors, status: 422
    end
  end

  def destroy 
     
    @teacher.destroy
  end
  private

  def set_teacher
    @teacher = User.find(params[:id])
  end

  def teacher_params
    params.require(:user).permit(:name, :nacionality, :state, :rg, :password, :birthday, :email, :role, :cpf)
  end
end

