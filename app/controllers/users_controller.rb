class UsersController < ApplicationController
  #Rota para cadastro do diretor
 before_action :set_user, only: [:show, :update, :destroy]
#  before_action :load_and_authorize_resource
  
  def index
    @users = User.where(role: "director")
    render json: @users
  end

  def show
    # @curriculum = School_curriculums.find(params[:id])
    # authorize! :read, @curriculum  
    # @coordinatorDP = User.where(role:"coordinatorDP")
    # authorize! :read, @coordinatorDP 
    # @coordinatorCourse = User.where(role:"coordinatorCourse")
    # authorize! :read, @coordinatorCourse
    # @schoolyear = Schoolyears.find(params[:id])
    # authorize! :read, @schoolyear

    render json: @user
  end

  def create
    @user = User.new(user_params)
    if @user.save
      render json: @user
    else
      render json: @user.errors, status: 422
    end



  #   @department = Department.new(department_params)
  #   authorize! :creat, @department 
  #   if @department.save
  #     render json: @department
  #   else
  #     render json: @department.errors, status: 422
  #   end


    
  #   @course = Course.new(course_params)
  #   authorize! :creat, @course 
  #   if @course.save
  #     render json: @course
  #   else
  #     render json: @course.errors, status: 422
  #   end

    
  #   @coordinatorCourse = User.new.where(role:3)
  #   authorize! :creat, @coordinatorCourse
  #   if @coordinatorCourse.save
  #     render json: @coordinatorCourse
  #   else
  #     render json: @coordinatorCourse.errors, status: 422
  #   end

    
  #   @coordinatorDP = User.new.where(role:2)
  #   authorize! :creat, @coordinatorDP 
  #   if @coordinatorDP.save
  #     render json: @coordinatorDP
  #   else
  #     render json: @coordinatorDP.errors, status: 422
  #   end

  # end

  
  # @schoolyear = Schoolyear.new(schoolyear_params)
  # authorize! :create, @schoolyear
  # if @schoolyear.save
  #   render json: @schoolyear
  # else
  #   render json: @schoolyear.errors, status: 422
  # end

end


  def update
    # authorize! :update, @user
    if @user.update(user_params)
      render json: @user
    else
      render json: @user.errors, status: 422
    end

  #   @schoolyear= Schoolyears.find(params[:id])
  #   authorize! :update, @schoolyear
  #   if @schoolyear.update(schoolyear_params)
  #     render json: @schoolyear
  #   else
  #     render json: @schoolyear.errors, status: 422
  #   end
  end

  def destroy 
     
    @user.destroy
    
  end



private

def set_user
  @user = User.find(params[:id])
end

def user_params
  params.require(:user).permit(:name, :nacionality, :state, :rg, :password, :birthday, :email, :role, :cpf)
end

def department_params
  params.require(:department).permit(:name, :code, :campus_address, :knowledge_field)
end

def course_params
  params.require(:course).permit(:name, :code, :campus_address, :knowledge_field)
end


def schoolyear_params
  params.require(:course).permit(:year, :status, :semester)
end

end