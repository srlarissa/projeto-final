class CoordinatorsController < ApplicationController
    #CoordenadorDP
    before_action :set_coordinator, only: [:show, :update, :destroy]
    # load_and_authorize_resource
 
    def index
      @coordinators = User.where(role: "coordinatorDP")
      render json: @coordinators
    end
  
    def show    
      render json: @coordinator
    end
  
    def create
      @coordinator = User.new(coordinator_params)
      if @coordinator.save
        render json: @coordinator
      else
        render json: @coordinator.errors, status: 422
      end
    end
  
    def update
      if @coordinator.update(coordinator_params)
        render json: @coordinator
      else
        render json: @coordinator.errors, status: 422
      end
    end
  
    def destroy 
      @coordinator.destroy
    end
    private
    
    def set_coordinator
      @coordinator = User.find(params[:id])
    end
    
    def coordinator_params
      params.require(:user).permit(:name, :nacionality, :state, :rg, :password, :birthday, :email, :role, :cpf)
    end
  end
  