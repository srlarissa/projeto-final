class CoursesController < ApplicationController
  before_action :set_course, only:[:show, :update, :destroy]
  # load_and_authorize_resource
  def index
    @courses = Course.all
    render json: @courses
  end

  def show
    if @course
      render json: @course
    else
      render json: @course.errors, status: 404
    end
  end

  def create
    @course = Course.new(course_params)

    if @course.save
      render json: @course, status: :created
    else
      render json: @course.errors, status: 422
    end
  end

  def update
    if @course.update(course_params)
      render json: @course
    else
      render json: @course.errors, status: 422
    end
  end

  def destroy
    @course.destroy
  end

  private

  def course_params
    params.require(:course).permit(:name, :knowledge_field, :code, :campus_address)
  end

  def set_course
    @course = Course.find(params[:id])
  end
end
