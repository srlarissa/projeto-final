class AuthController < ApplicationController
  def login
    @user = User.find_by!(cpf: login_params[:cpf])

    if @user.authenticate(login_params[:password])
      #cria o token de authentication e envia para o front
      token = JsonWebToken::Base.encode(user_id: @user.id)

      render json: {token: token, user: UserSerializer.new(@user)}
    else
      render json: { message: "Password Incorrect" }, status: 401
    end
  end

  private

  def login_params
    params.require(:user).permit(:cpf, :password)
  end
end

